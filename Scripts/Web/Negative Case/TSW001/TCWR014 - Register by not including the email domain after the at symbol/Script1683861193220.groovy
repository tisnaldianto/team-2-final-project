import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Object Repository/Web/Register/button_BuatAkun'))

WebUI.setText(findTestObject('Web/Register/field_inputNama'), 'Tisnaldianto')

WebUI.setText(findTestObject('Object Repository/Web/Register/field_inputTanggallahir'), '02-Jul-1998')

WebUI.setText(findTestObject('Web/Register/field_inputEmail'), 'tisnaldianto@')

WebUI.setText(findTestObject('Object Repository/Web/Register/field_inputWA'), '081286989010')

WebUI.setEncryptedText(findTestObject('Object Repository/Web/Register/field_inputKatasandi'), 'dwvqmYkmTGQiy6koxAx+6g==')

WebUI.setEncryptedText(findTestObject('Object Repository/Web/Register/field_inputKonfirmasiKatasandi'), 'dwvqmYkmTGQiy6koxAx+6g==')

WebUI.click(findTestObject('Object Repository/Web/Register/Checkbox'))

WebUI.click(findTestObject('Object Repository/Web/Register/button_Daftar'))

WebUI.scrollToElement(findTestObject('Web/Register/text-buatAkun'), 0)

WebUI.takeScreenshot()

