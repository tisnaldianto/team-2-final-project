<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>randomclick</name>
   <tag></tag>
   <elementGuidId>80711dcc-117c-42a5-a83e-0525febb3a8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.author-box > div.row.justify-content-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b5d3d146-c11d-4a7a-b339-f4cb04f4d6db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row justify-content-center</value>
      <webElementGuid>c80a3593-25ce-4e97-8a7a-199918f82dd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                </value>
      <webElementGuid>c18f10d8-e3f2-4e59-8d17-44fcf5b20aab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]</value>
      <webElementGuid>d6bbc280-7ab2-49f0-8889-33b56a621dcd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div</value>
      <webElementGuid>90584830-487b-4737-bebf-925fa569a5e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[6]</value>
      <webElementGuid>08190e1a-e1eb-4540-aff8-662db6fa5ca2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div</value>
      <webElementGuid>4b6fbe77-2f82-48ca-910a-9595605a0b5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                ' or . = '
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                ')]</value>
      <webElementGuid>214fb6e4-7936-4ecf-8bee-6b60059f621d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
