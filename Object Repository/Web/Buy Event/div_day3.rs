<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_day3</name>
   <tag></tag>
   <elementGuidId>68359a26-cace-4c9a-88bc-d07d54284ca8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.blockHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='blockListEvent']/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ae6e2775-742b-48e3-a055-7471b70e0a14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>blockHeader</value>
      <webElementGuid>e5dc469a-16ed-4995-beba-7502ec73281b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        </value>
      <webElementGuid>b09eab61-666a-4ecd-b942-d1aed907433f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockHeader&quot;]</value>
      <webElementGuid>db8d6666-3755-4841-ae28-de5ebd6a1906</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='blockListEvent']/a/div/div</value>
      <webElementGuid>a7a9f315-2006-417a-9157-46f71230c8c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/following::div[6]</value>
      <webElementGuid>03f1e128-9b4f-4908-8a24-a2d5e80bfd13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::div[11]</value>
      <webElementGuid>887a0bbb-58a1-4821-a602-2011339a107e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div</value>
      <webElementGuid>9df11cd0-ad44-47e4-933b-562752815ae2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        ' or . = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        ')]</value>
      <webElementGuid>cc05a6cb-11da-4ffe-874a-32ba9a4fdcb0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
